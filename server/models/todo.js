var mongoose = require('mongoose');

var Todo = mongoose.model('Todo', {
  text: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt: {
    type: Number,
    default: null
  }

});

module.exports = {Todo};

// var newTodo = new Todo({
//   text: 'Go to gym',
//   completed: true,
//   completedAt: 5000
// });
//
// newTodo.save().then((doc) =>{
//   console.log('Saved todo:');
//   console.log(JSON.stringify(doc, undefined, 2));
// }, (e) => {
//   console.log('Unable to save todo')
// });
