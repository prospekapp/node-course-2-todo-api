var mongoose = require('mongoose');

// User model section
var User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  }

});


// var newUser = new User({
//   email: 'daghosta@gmail.com'
// });
//
// newUser.save().then((doc) =>{
//   console.log('Saved User');
//   console.log(JSON.stringify(doc, undefined, 2));
// }, (e) => {
//   console.log('Unable to save user')
// });

module.exports = {User};
