// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

var obj = new ObjectID();

console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db)=>{
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }
  console.log('Connected to MongoDB server');

db.collection('Users').find({name: "Moe"}).count().then((count) =>{
  console.log('User count: ' +count);
}, (err)=>{
  console.log('Unable to fetch!', err);
});


  // db.close();
} );
